/**
 * JavaScript file for Quote Generator API
 */
 "use strict";
 (function() {

   // MODULE GLOBAL VARIABLES AND CONSTANTS CAN BE PLACED HERE
   const BASE_URL = "http://api.forismatic.com/api/1.0/";
   const PROXY_URL = "https://calm-springs-86046.herokuapp.com/";
   let quoteContainer = qs(".quote-container");
   let quoteText = qs("#quote");
   let authorText = qs("#author");
   let twitterBtn = qs(".twitter-btn");
   let newQuoteBtn = qs("#new-quote");
   let loader = qs(".loader");

   window.addEventListener("load", init);

   /**
    * Listen for newQuote and twitterBtn event click
    */
   function init() {
     // THIS IS THE CODE THAT WILL BE EXECUTED ONCE THE WEBPAGE LOADS
     makeRequest();
     newQuoteBtn.addEventListener('click', makeRequest);
     twitterBtn.addEventListener('click', tweetQuote);
   }

   /**
    * Setup makeRequest function to run the fetch pipeline
    */
   function makeRequest() {
     let url = PROXY_URL + BASE_URL + "?method=getQuote&lang=en&format=json";
     fetch(url)
       .then(checkStatus)
       .then(resp => resp.json())
       .then(processData)
       .catch(handleError);
   }

   /**
    * Setup processData function to process data that are received from the previous
    * fetch chain.
    * @param {Object} data - use as a parameter
    */
   function processData(data) {
    showLoader();
    // If author is blank, then add unknown author
     if (data.quoteAuthor === '') {
       authorText.textContent = 'Unknown';
     } else {
       authorText.textContent = data.quoteAuthor;
     }
    // Reduce font size for long quote
    if (data.quoteText.length > 120) {
      quoteText.classList.add('long-quote');
    } else {
      quoteText.classList.remove('long-quote');
    }
     quoteText.textContent = data.quoteText;
     removeLoader();
   }

   /**
    * Handle an error that might occur in the fetch pipeline (e.g. a bad status code)
    * @param {Object} err - use as a parameter
    */
   function handleError(err) {
     console.error(err);
   }

   /**
    * Setup twitterQuote function that respond to when it gets clicked
    */
   function tweetQuote() {
     const quote = quoteText.textContent;
     const author = authorText.textContent;
     const twitterUrl = `https://twitter.com/intent/tweet?text=${quote} - ${author}`;
     window.open(twitterUrl, '_blank');
   }

   /**
    * Setup function to show loading animation
    */
   function showLoader() {
     loader.hidden = false;
     quoteContainer.hidden = true;
   }

   /**
    * Setup function to remove the loading animation
    */
   function removeLoader() {
     if (!loader.hidden) {
       quoteContainer.hidden = false;
       loader.hidden = true;
     }
   }

   /** ------------------------------ Helper Functions  ------------------------------ */
   /**
    * Helper function to check if a fetch response has a 200-level status code.
    * If so, returns the response object back for processing.
    * Otherwise, throws an Error with response status details to be caught in the
    * catch stateement of the fetch Promise chain.
    * @param {object} response - fetch response with status
    * @returns {object} - response same fetch response to be processed down the
    * fetch pipeline after this status check.
    * @throws {Error} if status code is < 200 or >= 300
    */
   function checkStatus(response) {
     if (response.ok) {
       return response;
     } else {
        throw Error("Error in request: " + response.statusText);
     }
   }

   /**
    * Returns the first DOM object that matches the given selector.
    * @param {string} selector - query selector.
    * @returns {object} The first DOM object matching the query.
    */
   function qs(selector) {
     return document.querySelector(selector);
   }
 })();