# Quote Generator

Build a random quote generator with Twitter integration using an API

## Getting Started

These instructions will give you a copy of the project up and running on
your local machine for development and testing purposes. See deployment
for notes on deploying the project on a live system.

## Built With

  - [JSDoc](https://jsdoc.app/) - Used
    for JSDoc commenting
  - [GNU General Public License](LICENSE.md) - Used to choose
    the license

## Authors

  - [Leon Xu](https://gitlab.com/leonxu260)

See also the list of
[contributors](https://github.com/PurpleBooth/a-good-readme-template/contributors)
who participated in this project.

## License

This project is licensed under the [GNU General Public License](LICENSE.md) - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

  - [Formismatic API](https://forismatic.com/en/api/)
